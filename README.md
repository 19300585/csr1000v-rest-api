# CSR1000v REST API

This repository contains python files to interact with the Cisco CSR1000V REST API.

## get-interface.py

A small python script which communicates with the CSR1000v through the REST API to retrieve information about the Loopback0 interface.

## put-interface.py

A small python script which changes the description and IP address of the CSR1000v through the REST API.